package com.esewa.springdemo;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import java.util.List;

public class Main {
    public static void main(String[] args) {
        ApplicationContext applicationContext = new ClassPathXmlApplicationContext("resource/applicationContext.xml");


        EmployeeDAO employeeDAO= (EmployeeDAO) applicationContext.getBean("employeeDAO");


        Employee employee = new Employee("Mohan",25000.00);
        employeeDAO.insert(employee);
        System.out.println("Data is inserted !!!!!");

        System.out.println(" Employee Information ");
        Employee employee1 = employeeDAO.findById(1);
        System.out.print(" ID : " + employee1.getEmployeeID() );
        System.out.print("  Name : " + employee1.getName() );
        System.out.println("   Age : " + employee1.getSalary());

        System.out.println("------Listing Multiple Records--------" );
        List<Employee> employees = employeeDAO.listEmployees();

        for (Employee employee2 : employees) {
            System.out.print("ID : " + employee2.getEmployeeID());
            System.out.print(", Name : " + employee2.getName() );
            System.out.println(", Age : " + employee2.getSalary());
        }

        System.out.println("----Updating Record with ID = 2 -----" );
        employeeDAO.update(2, 50000.00);


        System.out.println("----Deleting Record with ID = 2 -----" );
        employeeDAO.delete(2);

    }
}
