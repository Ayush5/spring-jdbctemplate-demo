package com.esewa.springdemo;

import java.util.List;

public interface  EmployeeDAO {
    public void insert(Employee employee);
    public Employee findById(int id);
    public void update(int id, double salary);
    public void delete(int id);
    public List<Employee> listEmployees();



}
