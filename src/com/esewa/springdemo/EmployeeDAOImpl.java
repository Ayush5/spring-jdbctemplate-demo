package com.esewa.springdemo;

import org.springframework.jdbc.core.JdbcTemplate;

import javax.sql.DataSource;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.List;

public class EmployeeDAOImpl implements EmployeeDAO {

    private DataSource dataSource;
    private JdbcTemplate jdbcTemplate;


    public void setDataSource(DataSource dataSource) {
        this.dataSource = dataSource;
        jdbcTemplate=new JdbcTemplate(dataSource);
    }

    public void insert(Employee employee) {
        String sql = "INSERT INTO employee( name , salary ) VALUES ( ?, ?)";

        jdbcTemplate.update(sql, new Object[]{
                employee.getName(), employee.getSalary()
        });

    }

    public Employee findById(int id){

            String sql = "select * from employee where employeeID = ?";

            Employee employee = jdbcTemplate.queryForObject(sql,
                    new Object[]{id}, new EmployeeMapper());

            return employee;
        }

    public List<Employee> listEmployees() {
        String SQL = "select * from employee";
        List<Employee> employees = jdbcTemplate.query(SQL, new EmployeeMapper());
        return employees;
    }

    public void update(int id , double salary){

            String SQL = "update employee set salary = ? where employeeID = ?";
            jdbcTemplate.update(SQL, salary, id);
            System.out.println("Updated Record with ID = " + id );
          }

    public void delete(int id) {
        String SQL = "delete from employee where employeeID = ?";
        jdbcTemplate.update(SQL, id);
        System.out.println("Deleted Record with ID = " + id );
        return;
    }
    }





